package main;

public class Radius {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++)
            System.out.println(circumference(i));
    }

    static double circumference(int num) {
        return 3.14 * 2 * num;
    }
}
